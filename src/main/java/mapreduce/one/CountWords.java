package mapreduce.one;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class CountWords {
	public static void main(String[] args) throws Exception {
        
        //------------- Read ------------------------
        if (args.length != 2) {
            System.err.println("Usage: ReadTwitter <input path> <output path>");
            System.exit(-1);
        }
        Job job = new Job();
        job.setJarByClass(mapreduce.one.CountWords.class);
        job.setJobName("Count WordsTrump");

        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));

        job.setMapperClass(mapreduce.one.MapPhase.class);
        job.setReducerClass(mapreduce.one.ReducePhase.class);
        
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);
        System.out.println("Done!");
        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }
}
